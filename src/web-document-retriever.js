const parser = new DOMParser();

export async function retrieve(url) {
  const http =new XMLHttpRequest();
  const resultPromise = new Promise((resolve) => {
    http.onreadystatechange= () => {
      if (http.readyState==4 && http.status==200) {
        const document = parser.parseFromString(http.responseText, 'text/html');
        resolve(document);
      }
    }
  });

  http.open('GET', url, true);
  http.send();

  return resultPromise;
}