import { toBahnDate } from './bahn-format'

function _toConnection(connection, selectedDateTimeUtc){
    const departureTimes = _toTimes(connection.getElementsByClassName('time')[0].textContent);
    const tr_last = connection.getElementsByClassName('last')[0];
    const arrivalTimes = _toTimes(tr_last.getElementsByClassName('time')[0].textContent);

    const detectedDepartTime = departureTimes[0].trim();
    const departDay = _determineDay(selectedDateTimeUtc, detectedDepartTime);

    return {
        departureDate: departDay,
        departureTime: detectedDepartTime,
        estimatedDepartureTime: departureTimes.length === 2 ? departureTimes[1].trim() : departureTimes[0].trim(),
        departureLocation: connection.getElementsByClassName('station first')[0].textContent.trim(),

        arrivalTime: arrivalTimes[0].trim(),
        estimatedArrivalTime: arrivalTimes.length === 2 ? arrivalTimes[1].trim() : arrivalTimes[0].trim(),
        arrivalLocation: connection.getElementsByClassName('station stationDest')[0].textContent.trim(),

        duration: connection.getElementsByClassName('duration')[0].textContent.trim(),
        type: connection.getElementsByClassName('products')[0].textContent.trim()
    }
}

function _determineDay(selectedDateTimeUtc, retrievedTime) {

    // selectedDateTimeUtc.getHours() will convert hours to local time zone
    if(_parseHour(retrievedTime) < selectedDateTimeUtc.getHours()) {
        var date = new Date(selectedDateTimeUtc);
        date.setDate(date.getDate() + 1);
        return toBahnDate(date);
    }
    return toBahnDate(selectedDateTimeUtc);
}

function _parseHour(timeStr) {
    return parseInt(timeStr.split(":")[0]);
}

function _toTimes(timesAsString){
    return timesAsString.replace('\n', '').split(/\s+/);
}

export function toConnections(document, searchDateTimeUtc) {
    const connectionsHtml = [].slice.call(document.getElementsByClassName('boxShadow  scheduledCon'));
    return connectionsHtml.map(conHtml => _toConnection(conHtml, searchDateTimeUtc));
}