export function toBahnDate(date) {
    // getDate() etc. convert to local time zone of the client
    return ('0' + date.getDate()).slice(-2) + '.'
            + ('0' + (date.getMonth()+1)).slice(-2) + '.'
            + date.getFullYear();
}
  
export function toBahnTime(date) {
    // getHours() etc. convert to local time zone of the client
    return ('0' + date.getHours()).slice(-2) + ':'
            + ('0' + date.getMinutes()).slice(-2)
}