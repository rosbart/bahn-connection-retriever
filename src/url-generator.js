import { toBahnDate, toBahnTime } from './bahn-format'
const defaultOrigin = 'https://cors-anywhere.herokuapp.com/';

class ConnectionQuery {

    constructor(){
        this.hasOrigin = false;
    }

    from(from) {
        this.from = from;
        return this;
    }

    to(to) {
        this.to = to;
        return this;
    }

    at(at) {
        this.at = at;
        return this;
    }

    origin(origin) {
        if(origin || '' === origin) { 
            this.hasOrigin = true;
        } else {
            this.hasOrigin = false;
        }
        this.origin = origin;
        return this;
    }
}

export function generate(query) {
    const bahnDate = toBahnDate(query.at);
    const bahnTime = toBahnTime(query.at);

    return `${query.hasOrigin ? query.origin : defaultOrigin}https://reiseauskunft.bahn.de/bin/query.exe/dn?revia=yes&existOptimizePrice=1&country=` +
        'DEU&dbkanal_007=L01_S01_D001_KIN0001_qf-bahn-svb-kl2_lz03&start=1&REQ0JourneyStopsS0A=' +
        `1&S=${query.from}&REQ0JourneyStopsSID=&Z=${query.to}&REQ0JourneyStopsZID=&date=${bahnDate}&time=${bahnTime}&timesel=depart&returnDate=` +
        '&returnTime=&returnTimesel=depart&optimize=0&auskunft_travelers_number=1&tariffTravellerType.1=' +
        'E&tariffTravellerReductionClass.1=0&tariffClass=2&rtMode=DB-HYBRID&externRequest=yes&HWAI=' +
        'JS%21js%3Dyes%21ajax%3Dyes%21';
}

export function connections() {
    return new ConnectionQuery();
}