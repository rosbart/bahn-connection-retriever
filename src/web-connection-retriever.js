import { connections } from './url-generator'
import { retrieve } from './web-document-retriever'
import { toConnections } from './connection-converter'

const searchDate = new Date(Date.UTC(2018, 11, 11, 20, 45, 0)); //11.12.2018 21:45 UTC+1

const url = connections()
            .from('Kiefersfelden')
            .to('München Hbf')
            .at(searchDate);

retrieve(url).then(doc => {
  toConnections(doc, searchDate).forEach(console.log);
})