import { toConnections } from '../src/connection-converter';
import { JSDOM } from 'jsdom';
import { readFile } from 'fs';


//const document = parser.parseFromString(http.responseText, 'text/html');

test('1 equals 1', () => {
    expect(1).toBe(1);
});

test('Interpret default correctly', async () => {
    const fileContent = await read(process.cwd() + '/assets/test_default.html');
    const dom = new JSDOM(fileContent).window.document;
    var searchDate = new Date(Date.UTC(2018, 11, 13, 8, 35, 0))
    const connections = toConnections(dom, searchDate);
    expect(connections.length).not.toBe(0);
    
    expect(connections).toContainEqual({
        "departureDate": "13.12.2018",
        "departureTime": "09:54",
        "estimatedDepartureTime": "",
        "departureLocation": "München Hbf",
        "arrivalTime": "10:30",
        "estimatedArrivalTime": "10:30",
        "arrivalLocation": "Ingolstadt Hbf",
        "duration": "0:36",
        "type": "ICE"
      });
    
      // this is the object we want to check
      expect(connections).toContainEqual({
        "departureDate": "13.12.2018",
        "departureTime": "10:05",
        "estimatedDepartureTime": "",
        "departureLocation": "München Hbf",
        "arrivalTime": "10:52",
        "estimatedArrivalTime": "10:52",
        "arrivalLocation": "Ingolstadt Hbf",
        "duration": "0:47",
        "type": "RE"
      });

      expect(connections).toContainEqual({
        "departureDate": "13.12.2018",
        "departureTime": "10:21",
        "estimatedDepartureTime": "",
        "departureLocation": "München Hbf",
        "arrivalTime": "10:58",
        "estimatedArrivalTime": "10:58",
        "arrivalLocation": "Ingolstadt Hbf",
        "duration": "0:37",
        "type": "ICE"
      });

});

test('Interpret day switch correctly', async () => {
    const fileContent = await read(process.cwd() + '/assets/test_next_day.html');
    const dom = new JSDOM(fileContent).window.document;
    var searchDate = new Date(Date.UTC(2018, 11, 11, 20, 45, 0)) //11.12.2018 21:45 UTC+1
    const connections = toConnections(dom, searchDate);
    expect(connections.length).not.toBe(0);
    
    expect(connections).toContainEqual({
        "departureDate": "11.12.2018",
        "departureTime": "22:02",
        "estimatedDepartureTime": "",
        "departureLocation": "Kiefersfelden",
        "arrivalTime": "23:15",
        "estimatedArrivalTime": "23:15",
        "arrivalLocation": "München Hbf",
        "duration": "1:13",
        "type": "M"
      });
    
      // this is the object we want to check
      expect(connections).toContainEqual({
        "departureDate": "11.12.2018",
        "departureTime": "23:02",
        "estimatedDepartureTime": "",
        "departureLocation": "Kiefersfelden",
        "arrivalTime": "00:57",
        "estimatedArrivalTime": "00:57",
        "arrivalLocation": "München Hbf",
        "duration": "1:55",
        "type": "M"
      });

      expect(connections).toContainEqual({
        "departureDate": "12.12.2018",
        "departureTime": "05:37",
        "estimatedDepartureTime": "",
        "departureLocation": "Kiefersfelden",
        "arrivalTime": "06:45",
        "estimatedArrivalTime": "06:45",
        "arrivalLocation": "München Hbf",
        "duration": "1:08",
        "type": "M"
      });

});

function read(filePath) {

    return new Promise((resolve, reject) => {
        console.log()
        readFile(filePath, (err, html) => {
            if(err) {
                reject(err);
            }
            resolve(html)
        });
    });
    
    
}