# bahn-connection-retriever

This project allows you to retrieve connections of Deutsche Bahn as for example:

```js
{   
    departureDate: '12.12.2018',
    departureTime: '14:43',
    estimatedDepartureTime: '14:43',
    departureLocation: 'München Hbf',
    arrivalTime: '15:54',
    estimatedArrivalTime: '15:54',
    arrivalLocation: 'Kiefersfelden',
    duration: '1:11',
    type: 'M' 
}
```

# How to use

## prerequisites

1. install npm
1. ```npm install https://gitlab.com/rosbart/bahn-connection-retriever.git#1.1.0```

## with a webbrowser

```js
import { connections, generate } from '@rosbart/bahn-connection-retriever/src/url-generator'
import { retrieve } from '@rosbart/bahn-connection-retriever/src/web-document-retriever'
import { toConnections } from '@rosbart/bahn-connection-retriever/src/connection-converter'

const searchDate = new Date(Date.UTC(2018, 11, 11, 20, 45, 0));

// construct query url
const url = generate(connections()
                    .from('Kiefersfelden')
                    .to('München Hbf')
                    .at(searchDate));

// request document and convert to connection
retrieve(url).then(doc => {
  toConnections(doc, searchDate).forEach(console.log);
})
```

## with node.js

```js
const { connections, generate } = require('@rosbart/bahn-connection-retriever/dist/url-generator');
const { toConnections } = require('@rosbart/bahn-connection-retriever/dist/connection-converter');


const searchDate = new Date(Date.UTC(2018, 11, 11, 20, 45, 0));

// construct query url
const url = generate(
    connections()
    .from('Ingolstadt Hbf')
    .to('München Hbf')
    .at(searchDate)
    .origin('')
);

// the request function is not part of this project
const document = await request(url);

toConnections(document, searchDate).forEach(console.log);
```

# More information

## Testing
Test assets are legally protected and therefore not part of this project.

## Maintainer

* [Phibart](https://gitlab.com/phibart)
* [Josch Rossa](https://gitlab.com/josros)
